#pragma once

#ifndef DECLARATION_H
#define DECLARATION_H

#include <vector>
#include <string>

#include "functions.h"

#include "TROOT.h"
#include "TRint.h"
#include "TTree.h"
#include "TChain.h"
#include "TNtuple.h"
#include "TFile.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TMath.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TFrame.h"

using std::vector;
using std::string;

TFile *myFiles[7];
TH1D *rADC[7];
TH1D *lADC[7];

// Nomenclature : from the distances points
Int_t dleft[7]  = { 50, 100, 150, 200, 300, 400, 500};
//Int_t dright[7] = {100, 200, 300, 400, 450, 500, 550};
Int_t dright[7] = {550, 500, 450, 400, 300, 200, 100};

vector<double> totLength = {50, 100, 150, 200, 300, 400, 450, 500, 550};
//double AveNPhoton[9];
//double AveNPhotonErr[9];



// Array for the fit parameters : Norm, Mean, Standard deviation
double meanR[7];
double meanL[7];
double meanER[7];
double meanEL[7];


#endif

