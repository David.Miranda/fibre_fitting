
#include <iostream>
#include <string>
#include <vector>

#include "TROOT.h"
#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TPad.h"
#include "TMath.h"
#include "TFormula.h"
#include "TBrowser.h"
#include "TStyle.h"
#include "TGraph.h"
#include "TCanvas.h"
#include "TGraphErrors.h"
#include "TFrame.h"
#include "TAttCanvas.h"
#include "TAxis.h"
#include "TPaveStats.h"

#include "functions.h"
#include "declarations.h"

using namespace std;


int main(int argc, char *argv[]) {

  if(argc != 3) {
    cout << "usage: BetterFit <root file #1> <fibre name>" << endl;
    return -1;
  }

  //Initializing the path to output
  std::string input_file(argv[1]);
  std::string input_fibre(argv[2]);

  TFile *outfile = new TFile(TFileName(input_file,input_fibre).c_str(),"RECREATE");
  gStyle->SetOptStat(0001);


  TFile *Input[7];

  string UsingFiles[7] = {
      "/Users/lonewolf/Mu3e/Fibres/Data/scsf78-05cm.root",
      "/Users/lonewolf/Mu3e/Fibres/Data/scsf78-10cm.root",
      "/Users/lonewolf/Mu3e/Fibres/Data/scsf78-15cm.root",
      "/Users/lonewolf/Mu3e/Fibres/Data/scsf78-20cm.root",
      "/Users/lonewolf/Mu3e/Fibres/Data/scsf78-30cm.root",
      "/Users/lonewolf/Mu3e/Fibres/Data/scsf78-40cm.root",
      "/Users/lonewolf/Mu3e/Fibres/Data/scsf78-50cm.root",
  };

//  string UsingFiles[7] = {
//      "/Users/lonewolf/Mu3e/Fibres/Data/scsf11-05cm.root",
//      "/Users/lonewolf/Mu3e/Fibres/Data/scsf11-10cm.root",
//      "/Users/lonewolf/Mu3e/Fibres/Data/scsf11-15cm.root",
//      "/Users/lonewolf/Mu3e/Fibres/Data/scsf11-20cm.root",
//      "/Users/lonewolf/Mu3e/Fibres/Data/scsf11-30cm.root",
//      "/Users/lonewolf/Mu3e/Fibres/Data/scsf11-40cm.root",
//      "/Users/lonewolf/Mu3e/Fibres/Data/scsf11-50cm.root",
//  };

//  string UsingFiles[7] = {
//      "/Users/lonewolf/Mu3e/Fibres/Data/scsf81-05cm.root",
//      "/Users/lonewolf/Mu3e/Fibres/Data/scsf81-10cm.root",
//      "/Users/lonewolf/Mu3e/Fibres/Data/scsf81-15cm.root",
//      "/Users/lonewolf/Mu3e/Fibres/Data/scsf81-20cm.root",
//      "/Users/lonewolf/Mu3e/Fibres/Data/scsf81-30cm.root",
//      "/Users/lonewolf/Mu3e/Fibres/Data/scsf81-40cm.root",
//      "/Users/lonewolf/Mu3e/Fibres/Data/scsf81-50cm.root",
//  };

    //  NOMEMCLATURE :
    //  i     left CH03 right CH01

    //  0      50mm     550mm
    //  1     100mm     500mm
    //  2     150mm     450mm
    //  3     200mm     400mm
    //  4     300mm     300mm
    //  5     400mm     200mm
    //  6     500mm     100mm

  for (int i=0 ; i<7 ; i++) {
      Input[i] = new TFile(UsingFiles[i].c_str());
      lADC[i] = (TH1D*)Input[i]->Get("ADC_CH03");
      lADC[i]->SetDirectory(0);
      lADC[i]->Rebin(1);
      lADC[i]->SetName(TString::Format("Left ADC : %02dcm",dleft[i]));

      rADC[i] = (TH1D*)Input[i]->Get("ADC_CH01");
      rADC[i]->SetDirectory(0);
      rADC[i]->Rebin(1);
      rADC[i]->SetName(TString::Format("Right ADC : %02dcm",dright[i]));

      outfile->cd();
      lADC[i]->Write(TString::Format("Left ADC : %02dcm",dleft[i]));
      rADC[i]->Write(TString::Format("Right ADC : %02dcm",dright[i]));
      Input[i]->Close();
  }

  gDirectory->pwd();

  ////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////////

  // ADC spectra for channel 01 and channel 03 at each length point
  TCanvas *cMyADCs = new TCanvas("MyADCs","ADC at each length per CH",1200,1200);
  TPad *padLeft  = new TPad("padLeft","padLeft",0.02,0.02,0.49,0.98,kTeal+5);
  TPad *padRight = new TPad("padLeft","padLeft",0.51,0.02,0.98,0.98,kAzure-9);
  padLeft->Draw();
  padRight->Draw();

  padLeft->cd();
  padLeft->Divide(2,4);
  for (int i=0 ; i<= 6 ; i++) {
      padLeft->cd(i+1);
      lADC[i]->Draw("hist");
  }

  padRight->cd();
  padRight->Divide(2,4);
  for (int i=0 ; i<=6 ; i++) {
      padRight->cd(i+1);
      rADC[6-i]->Draw("hist");
  }

  cMyADCs->Write();
  cMyADCs->Print("myADCs.pdf");

  ////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////////

  //Fitting with Poison/Gauss distribution
  Double_t xlow  = 0.00;
  Double_t xhigh = 30.0;

  //Define the Poison Dist.
  TF1 *fPoisson = new TF1("myPoisson",poissonf,xlow,xhigh,2);
  fPoisson->SetParameters(2000,10);
  fPoisson->SetParNames("Amplitude","Mean");

  TCanvas *cMyADCsFit = new TCanvas("MyADCsFit","Fit : ADC at each length per CH",1200,1200);
  TPad *FitpadLeft  = new TPad("FitpadLeft","FitpadLeft",0.02,0.02,0.49,0.98,kTeal+5);
  TPad *FitpadRight = new TPad("FitpadLeft","FitpadLeft",0.51,0.02,0.98,0.98,kAzure-9);
  FitpadLeft->Draw();
  FitpadRight->Draw();

  FitpadLeft->cd();
  FitpadLeft->Divide(2,4);
  for (int i=0 ; i<= 6 ; i++) {
      FitpadLeft->cd(i+1);
      lADC[i]->SetStats(1);
//      lADC[i]->Rebin(2);
//      lADC[i]->Draw("SAME");
      lADC[i]->Fit("myPoisson","RL");
      lADC[i]->Write();
      TF1 *fit = lADC[i]->GetFunction("myPoisson");
      meanL[i] = fit->GetParameter(1);
      meanEL[i] = fit->GetParError(1);
  }

  FitpadRight->cd();
  FitpadRight->Divide(2,4);
  for (int i=0 ; i<=6 ; i++) {
      FitpadRight->cd(i+1);
      rADC[6-i]->SetStats(1);
//      rADC[6-i]->Rebin(2);
//      rADC[6-i]->Draw("SAME");
      rADC[6-i]->Fit("myPoisson","RL");
      rADC[6-i]->Write();
      TF1 *fit = rADC[6-i]->GetFunction("myPoisson");
      meanR[6-i] = fit->GetParameter(1);
      meanER[6-i] = fit->GetParError(1);
  }

  cMyADCsFit->Write();
  cMyADCsFit->Print("myADCsFit.pdf");

  //Fitting with Poison + Gauss distribution // TO DO

  ////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////////

  // ATTENUATION : Number of Photo-electrons vs length

  //Define the Linear Fit function.
  TF1 *myLinear = new TF1("myLinear",Linearf,totLength[0],totLength[8],2);
  myLinear->SetParameters(10,-1);
  myLinear->SetParNames("Log(b)","lambda");

  vector<double> Llength;
  vector<double> Rlength;
  vector<double> lengthErr;

  double AveNPhoton[9] = {
      meanL[0],
      (meanL[1]+meanR[6])*0.5,
      meanL[2],
      (meanL[3]+meanR[5])*0.5,
      (meanL[4]+meanR[4])*0.5,
      (meanL[5]+meanR[3])*0.5,
      meanR[2],
      (meanL[6]+meanR[1])*0.5,
      meanR[0],
  };

  double AveNPhotonErr[9] = {
      meanEL[0],
      TMath::Sqrt(2)/(TMath::Power(meanEL[1]-AveNPhoton[1],2) + TMath::Power(meanER[6]-AveNPhoton[1],2)),
      meanEL[2],
      TMath::Sqrt(2)/(TMath::Power(meanEL[3]-AveNPhoton[3],2) + TMath::Power(meanER[5]-AveNPhoton[3],2)),
      TMath::Sqrt(2)/(TMath::Power(meanEL[4]-AveNPhoton[4],2) + TMath::Power(meanER[4]-AveNPhoton[4],2)),
      TMath::Sqrt(2)/(TMath::Power(meanEL[5]-AveNPhoton[5],2) + TMath::Power(meanER[3]-AveNPhoton[5],2)),
      meanER[2],
      TMath::Sqrt(2)/(TMath::Power(meanEL[6]-AveNPhoton[7],2) + TMath::Power(meanER[1]-AveNPhoton[7],2)),
      meanER[0],
  };

  for (int k=0 ; k<7 ;k++) {
      Llength.push_back( (double)dleft[k] );
      Rlength.push_back( (double)dright[k] );
      lengthErr.push_back(0);
  }

  TGraphErrors *grAtteL = new TGraphErrors(Llength.size(),&Llength[0],&meanL[0],0,&meanEL[0]);
  grAtteL->SetTitle("Attenuation :  Left side - CH03");
  grAtteL->SetMarkerColor(4);
  grAtteL->SetMarkerStyle(21);
  grAtteL->GetXaxis()->SetTitle("Length [mm]");
  grAtteL->GetYaxis()->SetTitle("Number of ph.e");
  grAtteL->Draw("ALP");

  TGraphErrors *grAtteR = new TGraphErrors(Rlength.size(),&Rlength[0],&meanR[0],0,&meanER[0]);
  grAtteR->SetTitle("Attenuation :  Right side - CH01");
  grAtteR->SetMarkerColor(4);
  grAtteR->SetMarkerStyle(21);
  grAtteR->GetXaxis()->SetTitle("Length [mm]");
  grAtteR->GetYaxis()->SetTitle("Number of ph.e");
  grAtteR->Draw("ALP");

  TGraphErrors *grAtteBoth = new TGraphErrors(totLength.size(),&totLength[0],&AveNPhoton[0],0,&AveNPhotonErr[0]);
  grAtteBoth->SetTitle("Attenuation :  Both Channels combined");
  grAtteBoth->SetMarkerColor(4);
  grAtteBoth->SetMarkerStyle(21);
  grAtteBoth->GetXaxis()->SetTitle("Length [mm]");
  grAtteBoth->GetYaxis()->SetTitle("Number of ph.e");
  grAtteBoth->Fit("myLinear","R");
  grAtteBoth->Draw("AP");
  TF1 *fit = grAtteBoth->GetFunction("myLinear");
  double slope = fit->GetParameter(1);
  double slopeErr = fit->GetParError(1);

  Printf("Attenuation length = %f +- %f log(photons)/mm", slope, slopeErr);

  grAtteL->Write("LeftAtte");
  grAtteR->Write("RightAtte");
  grAtteBoth->Write("FulltAtte");

  gStyle->SetOptFit(0111);
  TCanvas *cAtte = new TCanvas("Attenution","Attenuation : both side",1200,1200);
  TPad *PadLeft  = new TPad("PadLeft" ,"Left side pad" ,0.00,0.66,1.00,1.00,kTeal+5);
  TPad *PadRight = new TPad("PadRight","Right side pad",0.00,0.33,1.00,0.66,kAzure-9);
  TPad *PadBoth = new TPad("PadBoth","Combined side pad",0.00,0.00,1.00,0.33,kYellow-7);
  PadLeft->Draw();
  PadRight->Draw();
  PadBoth->Draw();


  PadLeft->cd();
  PadLeft->SetLogy();
  grAtteL->Draw("AP");

  PadRight->cd();
  PadRight->SetLogy();
  grAtteR->Draw("AP");

  PadBoth->cd();
  PadBoth->SetLogy();
  grAtteBoth->Draw("AP");

  TPaveStats* myPaveStats=(TPaveStats
  *)(grAtteBoth->GetListOfFunctions()->FindObject("stats"));
  myPaveStats->SetX1NDC(.55);
  myPaveStats->SetX2NDC(.85);
  myPaveStats->SetY1NDC(.65);
  myPaveStats->SetY2NDC(.85);
  myPaveStats->SetTextColor(4);
  gPad->Modified();

  cAtte->Write("Attenuation");
  cAtte->Print("Attenuation.pdf");

  ////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////////

  for(int k=0 ; k<lengthErr.size(); k++) {
      Printf( "%d : %f +- %f", dleft[k], meanL[k], meanEL[k]);
  }
  cout << "--"<< endl;
  for(int k=0 ; k<lengthErr.size(); k++) {
      Printf( "%d : %f +- %f", dright[6-k], meanR[6-k], meanER[6-k]);
  }
  cout << "--"<< endl;
  for(int k=0 ; k<totLength.size(); k++) {
      Printf( "%f : %f +- %f", totLength[k], AveNPhoton[k], AveNPhotonErr[k]);
  }

  ////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////////

  outfile->Write();

} //END of Program
