#pragma once

#ifndef __FUCNTIONS_H_INCLUDED__
#define __FUCNTIONS_H_INCLUDED__

#include <vector>
#include <string>

#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TF1.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TFrame.h"
#include "TRandom.h"
#include "TMath.h"

#include "declarations.h"

using std::string;
using std::to_string;
using std::vector;

// Sets the same path address, name, and correct extension of the ROOT file
string TFileName(string inputN, string inputF) {
  string fname = inputN;
  fname.erase(fname.find_last_of("/"),string::npos);
  fname = operator+(fname,"/");
  fname = operator+(fname,inputF);
  fname = operator+(fname,"_attenuation.root");
  return fname;
}

// Poison Distribution
//  parameter   name        description
//  0           norm        Normalisation
//  1           mu          mean of the distribution
//  2           sigma       width of the distribution
Double_t poissonf(Double_t*x,Double_t*par)
{
    Double_t PDF = 0.0;
    PDF = par[0]*TMath::Poisson(x[0],par[1]);
    return PDF;
}

Double_t Linearf(Double_t*x,Double_t*par)
{
    Double_t Line = 0.0;
    Line = par[0] + x[0]*par[1];
    return Line;
}


#endif
